# Identity Playground

This project demonsatrates the experimental implementation of ERC-725, ERC-734, ERC-735.

## Development

### Complile
```
truffle compile
```

### Deploy contracts
1. run `Ganache.app`
2. `truffle migrate`


### Console
```
truffle console
```
