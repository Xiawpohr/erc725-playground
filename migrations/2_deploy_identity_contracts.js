const Identity = artifacts.require("Identity")
const KeyManager = artifacts.require("KeyManager")
const ClaimHolder = artifacts.require("ClaimHolder")

module.exports = function(deployer) {
  deployer.then(() => deployContracts(deployer))
}

async function deployContracts(deployer) {
  await deployer.deploy(Identity)
  await deployer.deploy(KeyManager)
  await deployer.deploy(ClaimHolder)
}
