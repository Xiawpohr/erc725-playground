const Identity = artifacts.require('Identity')
const KeyManager = artifacts.require('KeyManager')
const ClaimHolder = artifacts.require('ClaimHolder')

module.exports = function() {
  createIdentityOwner()
    .then(owner => console.log('owner address: ', owner.address))
}

async function createIdentityOwner () {
  const accounts = await web3.eth.getAccounts()
  const ownerAddress = accounts[0]

  Identity.defaults({ from: ownerAddress })
  KeyManager.defaults({ from: ownerAddress })
  ClaimHolder.defaults({ from: ownerAddress })

  const owner = await Identity.new()
  const keyManager = await KeyManager.new()
  const claimHolder = await ClaimHolder.new()

  const data734 = web3.utils.keccak256('734')
  const data735 = web3.utils.keccak256('735')
  // await owner.setData(data734, web3.utils.padLeft(keyManager, 32))
  // await owner.setData(data735, web3.utils.padLeft(claimHolder, 32))

  return owner
}
