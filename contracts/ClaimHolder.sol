pragma solidity >= 0.4.21 < 0.6.0;

import "./ERC735.sol";

contract ClaimHolder is ERC735 {

  mapping (bytes32 => Claim) claims;
  mapping (uint256 => bytes32[]) claimIdsByTopic;

  function getClaim(bytes32 _claimId) 
    external 
    view 
    returns (
      uint256 topic,
      uint256 schema,
      address issuer,
      bytes memory signature,
      bytes memory data,
      string memory uri
    )
  {
    return (
      claims[_claimId].topic,
      claims[_claimId].schema,
      claims[_claimId].issuer,
      claims[_claimId].signature,
      claims[_claimId].data,
      claims[_claimId].uri
    );
  }

  function getClaimIdsByTopic(uint256 _topic)
    external
    view
    returns (
      bytes32[] memory claimIds
    )
  {
    return claimIdsByTopic[_topic];
  }

  function addClaim(
    uint256 _topic,
    uint256 _schema,
    address _issuer,
    bytes calldata _signature,
    bytes calldata _data,
    string calldata _uri
  )
    external
    returns (bytes32 claimRequestId) 
  {
    bytes32 claimId = keccak256(abi.encodePacked(_issuer, _topic));

    if (claims[claimId].issuer != _issuer) {
        claimIdsByTopic[_topic].push(claimId);
    }

    claims[claimId] = Claim({
      topic: _topic,
      schema: _schema,
      issuer: _issuer,
      signature: _signature,
      data: _data,
      uri: _uri
    });

    emit ClaimAdded(
      claimId,
      _topic,
      _schema,
      _issuer,
      _signature,
      _data,
      _uri
    );

    return claimId;
  }

  function changeClaim(
    bytes32 _claimId,
    uint256 _topic,
    uint256 _schema,
    address _issuer,
    bytes calldata _signature,
    bytes calldata _data,
    string calldata _uri
  ) 
    external
    returns (bool success)
  {
    claims[_claimId] = Claim({
      topic: _topic,
      schema: _schema,
      issuer: _issuer,
      signature: _signature,
      data: _data,
      uri: _uri
    });
    
    emit ClaimChanged(
      _claimId,
      _topic,
      _schema,
      _issuer,
      _signature,
      _data,
      _uri
    );

    return true;
  }

  function removeClaim(bytes32 _claimId)
    external
    returns (bool success)
  {
    Claim memory cache = claims[_claimId];
    delete claims[_claimId];

    emit ClaimRemoved(
      _claimId,
      cache.topic,
      cache.schema,
      cache.issuer,
      cache.signature,
      cache.data,
      cache.uri
    );

    return true;
  }
}
